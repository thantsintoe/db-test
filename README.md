# How to test
```
npm install
```

```
npm run prepare
```

```
node build/nedb/nedb.js
node build/tingodb/tingodb.js
node build/tydb/tydb.js
```

# NeDB
## Advantages
- Easy to use
- Good API and docs
- Support typescript
- Support flatfile

## Disadvantages
- Each collection generates a *.db file. So, there will be 2 *.db files for 2 collections (eg. Cycles, Nodes)
- Data is not encrypted


# tingoDB
## Advantages
- Relative easy to use. Similar API as MongoDB
- Work with typescript
- Support flatfile

## Disadvantages
- Each collection generates a db file. So, there will be 2 db files for 2 collections (eg. Cycles, Nodes)
- Data is not encrypted

# tyDB
## Advantages
- Lightweight, built-in ODM
- written in typescript, strongly-typed database experience with an API and a query language similar to MongoDB
- In-memory or file persistence
- Based on NeDB

## Disadvantages
- Generate 2 files for data persistence. data file and index file.
- File system persistence is appending only

# teDB
## Advantages
- A TypeScript Embedded Database

## Disadvantages
- Have to write our own Storage Driver.
- Slightly more complex than others
- Documentation not so good

# UltraDB
## Advantages
...
## Disadvantages
- Not good enough.