import Datastore from 'nedb-promises'
import data from '../data.json'

let CycleCollection = Datastore.create('nedb-cycle.db')

async function run() {
    CycleCollection.ensureIndex({ fieldName: 'counter', unique: true });
    for (let i = 0; i < data.cycleInfo.length; i++) {
        CycleCollection.insert({ ...data.cycleInfo[i], data: new Date() })
            .then(data => {
                console.log('Inserted', data)
            })
            .catch(e => {
                console.log('Error', e)
            })
    }
    let foundCycle = await CycleCollection.find({
        counter: 2
    })
    console.log('Queried cycle', foundCycle)
}

run()

