let Db = require('tingodb')().Db
let db = new Db('tingodb', {});
let data = require('../data.json')

async function run() {
  let db = new Db('./', {});
  let CycleCollection = db.collection("db/tingodb-cycle");

  // Store each cycle records
  for (let i = 0; i < data.cycleInfo.length; i++) {
    CycleCollection.insert([data.cycleInfo[i]], { w: 1 }, function (err: any, result: any) {
      if (err) console.log(err)
    })
  }

  // Query cycle record
  CycleCollection.findOne({
    counter: 2
  }, (err: any, result: any) => {
    if (err) console.log(err)
    console.log('Queried cycle', result)
  })
}

run()


