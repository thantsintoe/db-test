import { Database, BaseModel, FS_Persistence_Adapter } from 'tydb'
import data from '../data.json'

class NodeModel extends BaseModel {
  id: string = ''
  ip: string = ''
  port: number = 0
  publicKey: string = ''
}

const NodeCollection = new Database<NodeModel>({
  ref: 'db/tydb-cycle',
  model: NodeModel,
  persistence_adapter: FS_Persistence_Adapter
})

async function run () {
  await NodeCollection.createIndex({ fieldName: 'publicKey', unique: true })

  for (let i = 0; i < data.nodeList.length; i++) {
    await NodeCollection.insert([NodeModel.new(data.nodeList[i])])
  }
  const foundNode = await NodeCollection.find({ filter: { port: 9002 } })
  console.log('foundNode', foundNode)
}

run()
