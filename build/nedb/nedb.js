"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nedb_promises_1 = __importDefault(require("nedb-promises"));
const data_json_1 = __importDefault(require("../data.json"));
let CycleCollection = nedb_promises_1.default.create('nedb-cycle.db');
async function run() {
    CycleCollection.ensureIndex({ fieldName: 'counter', unique: true });
    for (let i = 0; i < data_json_1.default.cycleInfo.length; i++) {
        CycleCollection.insert({ ...data_json_1.default.cycleInfo[i], data: new Date() })
            .then(data => {
            console.log('Inserted', data);
        })
            .catch(e => {
            console.log('Error', e);
        });
    }
    let foundCycle = await CycleCollection.find({
        counter: 2
    });
    console.log('Queried cycle', foundCycle);
}
run();
//# sourceMappingURL=nedb.js.map