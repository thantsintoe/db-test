"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const tydb_1 = require("tydb");
const data_json_1 = __importDefault(require("../data.json"));
class NodeModel extends tydb_1.BaseModel {
    constructor() {
        super(...arguments);
        this.id = '';
        this.ip = '';
        this.port = 0;
        this.publicKey = '';
    }
}
const NodeCollection = new tydb_1.Database({
    ref: 'db/tydb-cycle',
    model: NodeModel,
    persistence_adapter: tydb_1.FS_Persistence_Adapter
});
async function run() {
    await NodeCollection.createIndex({ fieldName: 'publicKey', unique: true });
    for (let i = 0; i < data_json_1.default.nodeList.length; i++) {
        await NodeCollection.insert([NodeModel.new(data_json_1.default.nodeList[i])]);
    }
    const foundNode = await NodeCollection.find({ filter: { port: 9002 } });
    console.log('foundNode', foundNode);
}
run();
//# sourceMappingURL=tydb.js.map